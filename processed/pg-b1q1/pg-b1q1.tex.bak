
        %this tex file was auto produced from TEI by lombardpress-print on 2015-12-24T14:15:02.359-05:00 using the  file:/Users/JCWitt/Desktop/lombardpress-print/lbp-latex-critical.xsl 
        \documentclass[twoside, openright]{article}
        
        % etex package is added to fix bug with eledmac package and mac-tex 2015
        % See http://tex.stackexchange.com/questions/250615/error-when-compiling-with-tex-live-2015-eledmac-package
        \usepackage{etex}
        
        %imakeidx must be loaded beore eledmac
        \usepackage{imakeidx}
        
        \usepackage{eledmac}
        \usepackage{titlesec}
        \usepackage [english]{babel}
        \usepackage [autostyle, english = american]{csquotes}
        \usepackage{geometry}
        \usepackage{fancyhdr}
        \usepackage[letter, center, cam]{crop}
        
        
        \geometry{paperheight=10in, paperwidth=7in, hmarginratio=3:2, inner=1.7in, outer=1.13in, bmargin=1in} 
        
        %fancyheading settings
        \pagestyle{fancy}
        
        %section headings
        \titleformat{\chapter}[display]
        {\normalfont\huge}{}{20pt}{\Huge}
      
        %quotes settings
        \MakeOuterQuote{"}
        
        %title settings
        \titleformat{\section} {\normalfont\scshape}{\thesection}{1em}{}
        \titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{12pt plus 2pt minus 2pt}
        \titleformat{\chapter} {\normalfont\Large\uppercase}{\thechapter}{50pt}{}
        
        
        %eledmac settings
        \foottwocol{B}
        \linenummargin{outer}
        \sidenotemargin{inner}
        
        %other settings
        \linespread{1.1}
        
        %custom macros
        \newcommand{\name}[1]{\textsc{#1}}
        \newcommand{\worktitle}[1]{\textit{#1}}
        
        
        \begin{document}
        \fancyhead[RO]{Librum I, Quaestio 1}
        \fancyhead[LO]{0.1.0}
        \fancyhead[LE]{Peter Gracilis}
        \chapter*{Librum I, Quaestio 1}
        
         
        \beginnumbering
         
        \bigskip
         \section*{Circa textum} 
        \pstart
        \ledsidenote{\textbf{1}}
         \edtext{\enquote{Cupientes aliquid de penuria}}{\Afootnote{Lombardus, I, prologus, xxx}} etc. Istud est prooemium libri sententiarum qui liber dividitur in prooemium et tractatum. Secunda incipit ibi \edtext{\enquote{veteris ac novae legis.}}{\Afootnote{Lombardus, I, prologus, xxx}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{2}}
        Prima potest dividi in quattuor partes principales in prima ponitur excusatio actoris in aggrediendo in secunda ponitur reprehensio invidorum in detrahendo. In tertia ponitur commendatio operis in prosequendo. In quarta ponitur exitatio auditorum in proficiendo. Secunda ibi \edtext{\enquote{quamvis non ambigamus.}}{\Afootnote{Lombardus, I, prologus, xxx}} Tertia ibi  horum igitur odibilem ecclesiam.  Quarta ibi \edtext{\enquote{non igitur haec labor debet.}}{\Afootnote{Lombardus, I, d. xxx}} etc.
        \pend
     
        \pstart
        \ledsidenote{\textbf{3}}
        Prima potest dividi in tres secundum quod \name{Magister}\index[persons]{Peter Lombard} ponit tres causas ipsum retrahentes a compilatione huius operis Prima est tenuitas suae scientiae. Secunda arduitas huius materiae mordacitas invidiae. In secunda ponit tres causas ipsum hortantes seu inducentes ad compilationem huius libri. Prima est exemplum pauperis viduae. Secunda species mercedis perpetuae. Tertia sunt praeces sociorum assiduae. In tertia comparat causas praedictas adinvicem ostendendo victoriam causarum mo?tium. Secunda incipit ibi \edtext{\enquote{consummationis fiduciam.}}{\Afootnote{Lombardus, I, prologus, xxx}} Tertia ibi \edtext{\enquote{quae vincit zelus}}{\Afootnote{Lombardus, I, prologus, xxx}} quaelibet aliarum causarum potest dividi sed pro nunc de divisione illud sufficiat.
        \pend
     
        \pstart
        \ledsidenote{\textbf{4}}
        Et ista ultima dividitur tres, nam in prima ponit contradictionis invidorum evidentiam, in secunda contradictionis causam, in tertia contradicentium nequitiam. Secunda ibi \edtext{\enquote{quia deficientibus volitum motibus.}}{\Afootnote{Lombardus, I, prologus, xxx}} Tertia ibi \edtext{\enquote{qui non rationi subiciunt}}{\Afootnote{Lombardus, I, prologus, xxx}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{5}}
        Tunc sequitur secunda pars principalis quamvis non ambigamus in qua ponitur detractorum duplex reprehensio. Prima est de impugnatione veritatis. Secunda de doctrina falsitatis. Secunda ibi quorum professio.
        \pend
     
        \pstart
        \ledsidenote{\textbf{6}}
        Sequitur tertia pars principalis horum igitur etc. in qua ponitur commendatio operis in prosequendo et quatturo facit hic quia primo commendat opus a causa finali, secundo a causa efficiente, tertio a materiali, et quarto a formali. Secunda ibi \edtext{\enquote{cum labore.}}{\Afootnote{Lombardus, I, prologus, xxx}} \ledsidenote{L12r} Tertia ibi \edtext{\enquote{ex testimoniis veritatis.}}{\Afootnote{Lombardus, I, prologus, xxx}} Quarta ibi \edtext{\enquote{in quo haereticae doctrinae}}{\Afootnote{Lombardus, I, prologus, xxx}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{7}}
        Tunc sequitur quarta et ultima pars principalis non ergo debet hic labor quae dividitur in tres secundum triplicem rationem ipsum auditorem excitantem ad proficiendum. Primo non inducitur ab utili brevitate, secundo ab humili veritate, tertio ab evidenti falsitate. Secunda ibi \edtext{\enquote{in hoc autem tractatu}}{\Afootnote{Lombardus, I, prologus, xxx}} Tertia ibi \edtext{\enquote{ut autem quod quaeritur.}}{\Afootnote{Lombardus, I, prologus, xxx}} Haec est divisio in generali prologi.
        \pend
     
        \pstart
        \ledsidenote{\textbf{8}}
        In speciali vero \name{Magister}\index[persons]{Peter Lombard} sic procedit quia primo ponit unam causam ipsum monentem ad libri compilationem quae est desiderium proficiendi et statim adiungit causam retrahentem quae est defectus ingenii et scientiae post ea ponit aliam causam retrahentem quae est altitudo materiae et magnitudo laboris et statim subiungit causam contraria monentem quae est confidentia auxilii divini et praemium auxilio promissum postea ponit victoriam causarum motium dicendo quod ista duo quae videntur ipsum retrahere a compilatione huius operis vincit amor intensus quem habet ad ecclesiam quo inflammatus opus aggreditur
        \pend
     
        \pstart
        \ledsidenote{\textbf{9}}
        In alia parte suam intensionem circa tria dicit versari primo circa fidei catholicae defensionem 2o circa veritatis theologicae explanationem 2o circa sacramentorum manifestationem postea ponit aliam causam motivitatem quae est caritatis et instantiam fratrum et sociorum est statim addit 3am causam contrariam retrahentem quae est mordacitas et contradictio invidiorum postea ponit contradictionis causam quae est discordia et deordinatio voluntatis ex qua provenit error et ex errore invidia et ex invidia contradictio oritur postea ponit contradicentium nequitiam quia scilicet voluntatem non subiciunt rationi nec sequuntur rationis rectitudinem nec auctoritati sacrae scripturae volunt se submittere sed tam rationi quam scripturae auctoritati secundum suum proponunt et quamdam superstitiosa religione exterius simulata habent argumentum ad ostendendum sapientiam novam quamdam doctrinam adinveniendo et quamdam clamorosa intentione inpugnant pertinaciter veritatem
        \pend
     
        \pstart
        \ledsidenote{\textbf{10}}
        postea assignat causas operis primo tangit fi?lem quae duplex ex scilicet falsitatis seu ecclesiae malorum eversio et veritatis catholicae ostensio postea ponit efficientem quae est duplex scilicet principalis quae est deus et instrumentalis ut ipse \name{Magister}\index[persons]{Peter Lombard} postea ponit materialem scilicet testimonia veritatis canonum et sanctorum postea tangit formalem quae est duplex scilicet formam tractatus quae consistit in divisione operis per liberos et capitula alia est forma tractandi quae est idem quod modus agendi postea dicit quod simul libris praemittit capitula ad faciliore et leviorem inductionem seu introductionem et sic terminatur sententia huius prologi Circa quem movetur talis quaestio
        \pend
      
        \bigskip
         \section*{[Quaestio]} 
        \bigskip
         \section*{[Rationes principales]} 
        \pstart
        \ledsidenote{\textbf{11}}
        \ledsidenote{L12r}Utrum quis viator scientifice sine dono speciali possit assentire theologicae omni vero supernaturali?
        \pend
     
        \pstart
        \ledsidenote{\textbf{12}}
        Quod sic primo quia omnis potentia naturalis potest [exire] in suam actionem naturalem, sed intellectus est potentia naturalis et sua operatio est scire et intelligere, ergo in ea potest [assentire] naturaliter sine fidei vel alterius luminis infusione.
        \pend
     
        \pstart
        \ledsidenote{\textbf{13}}
        Confirmatur quia solo rationis dictamine potest assentire quis, aliter vero igitur cuilibet. Patet antecedens et consequentia probatur, tum quia sensus aeque primo potest habere notitiam cuiuslibet sensibilis, cum  secundo quia omnia vera unicam rationem habent agendi in ipsum intellectum, igitur omnia ab ipsa aeque primo sicut apta nata cognosci.
        \pend
     
        \pstart
        \ledsidenote{\textbf{14}}
        Secundo haereticus damnatus et diabolus possunt assentire theologicae \ledsidenote{L12v} veritati sine speciali lumen, igitur et viator. Antecedens patet de dicente epulatione \edtext{\worktitle{Lucae}\index[works]{Lucas} 16.}{\Afootnote{Lucas 16, xxx}} Tum secundo quia agente et patiente sufficienter applicatis, aequalis sequitur actio. Sed intellectus agens est potens  omnia facere et intellectus possibilis omnia fieri ut patet \edtext{I \worktitle{De anima}\index[works]{de Anima}}{\Afootnote{Aristoteles, De anima, I, xxx}} ergo in homine approximatis aequaliter, sequitur actio in eo.
        \pend
     
        \pstart
        \ledsidenote{\textbf{15}}
        Tertio si quaestio foret falsa, sequitur quod tale lumen seu donum speciale infunderetur cuilibet theologo. Consequens falsum, quia nullus modernus theologus illud percipit sibi infundi. Consequentia patet, quia impossibile est haberi talia lumina seu tales habitus et habentem latere ut arguit \edtext{\name{Thomas Argentiae}\index[persons]{Thomas of Strasbourg} quaestione secunda prologi, articulo primo.}{\Afootnote{Thomas de Argentina, I, prologus, q. 2, a. 1}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{16}}
        Quarto viator potest convincere solo rationis dictamine aliam vitam futuram post istam, ergo. Consequentia tenet cum istud sit verum theologicum. Antecedens patet quia quam inditum est lapidi quietari in aliquo ultimate finaliter tam inditum est homini. Sed lapidi est inditum quod in aliquo aliquibus adeptis non potest non quietari, ergo erit sic de viatore. Minor patet. Maior probatur quia non minus ordinatur homo ad finem suum naturalem quam lapis. Tunc ultra homini est inditum naturaliter finaliter quietari in aliquo quo vel quibusque adeptis non potest non quietari. Sed nec tale est in praesenti vita, ergo restat alia iuta pro hominis quietatione. Maior patet dictam et minor patet de se et per illud \name{Augustini}\index[persons]{Augustinus} I \worktitle{Confessionum}\index[works]{Confessions} capitulo primo, \edtext{\enquote{ad te Domine nos fecisti et inquietum est cor nostrum donec requiescat in te.}}{\lemma{ad \dots\ te.}\Afootnote{Augustinus, Confessionum, c. 1}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{17}}
        In oppositum est \name{Augustinus}\index[persons]{Augustinus} \worktitle{Super Genesem}\index[works]{} dicens Maior est huius scripturae auctoritas quam omnis humani ingenii perspicacitas. Item \name{Magister}\index[persons]{Peter Lombard}, in prologo, dicens  \edtext{\enquote{ardua scandere, opus ultra vires praesumpsimus agere.}}{\Afootnote{Lombard, Sententia, I, prologus, 1 }} Item, \name{Augustinus}\index[persons]{Augustinus} II libro \worktitle{De sermone Domini in monte}\index[works]{} \edtext{\enquote{cum rationalis anima cogitat ac ratiocinatur quidquid in ea ratione, verum est non est ei attribuendum sed ipsi luci veritatis.}}{\lemma{cum \dots\ veritatis.}\Afootnote{Augustinus, De sermone Domini in monte, II, xxx}} Item nullus potest sapientiam creatam adipisci nisi gratia quae Deus est fuerit illustratus, ergo. Consequentia patet et antecedens est \edtext{\name{Augustini}\index[persons]{Augustinus} \worktitle{83 quaestiones}\index[works]{De diversis quaestionibus octaginta tribus}, quaestione 24.}{\Afootnote{Augustinus, De Diversis Quaestionibus, q. 24}} 
        \pend
      
        \bigskip
         \section*{[Prima Conclusio]} 
        \pstart
        \ledsidenote{\textbf{18}}
        Prima conclusio: licet assensus theologicus a propositione theologica non sit distinctus, tamen stat duas propositiones eiusdem subiecti vel praedicati idem significantes formari et unam ab alia specifice distingui. Prima pars patet, quia si distingueretur, sequeretur quod assensus theologicus posset esse sine propositione theologica. Consequens falsum quia tunc quis posset assentire sine notitia complexa, quod repugnat veritati. Secunda pars probatur quia, si ponatur quod fidelis formet illam propositionem \enquote*{Christus est passus} et infidelis etiam eadem quoad terminos, tunc sic fidelis assentit siglo propositionis per eum formante et infidelis non assentit, igitur specie distinguitur. Patet consequentia quia una realiter et intrinsece esse assensus et alia non, et tamen habent idem subiectum et praedicatum. Ergo conclusio vera.
        \pend
     
        \pstart
        \ledsidenote{\textbf{19}}
        Primum corollarium: quod stat hominem totam Sacram Scripturam scire et ipsum theologum non esse. Patet, si infidelis sciret scripturam, non esset theologus, quia non assentiret. Confirmatur quia, si fidelis viator sciret et formaret omnes propositiones quibus assentit, infidelis non propter hoc esset infidelis, quia eis non consentiret. Ergo a simili, sciens Sacram Scripturam et ei dissentiens, non est theologus.
        \pend
     
        \pstart
        \ledsidenote{\textbf{20}}
        Pro quo notandum quod infidelitas esse assensus respectu falsi vel dissensus respectu veri determinati per Ecclesiam aut in Scriptura Sacra revelati.
        \pend
     
        \pstart
        \ledsidenote{\textbf{21}}
        Secundum corollarium: quod stat viatorem habitu theologico informari et ipsum theologum non denominari. Ex quo ultra potest inferi et concedi quod de lege ordinata \ledsidenote{L13r} theologia potest manere fide, sed non potest manere seu stare sine fide theologia. Ex quo ultra sequitur quod non est tam essentiale habitui theologico esse \enquote*{theologicum} quam est ei essentiale esse \enquote*{habitum}.
        \pend
     
        \pstart
        \ledsidenote{\textbf{22}}
        Tertium corollarium: quod una propositio indistincta potest habere duo contra dictoria in specie distincta; patet quia contradictorum alicuius propositionis potest esse assensus in mente unius et dissensus immediate alterius.
        \pend
      
        \bigskip
         \section*{[Secunda Conclusio]} 
        \pstart
        \ledsidenote{\textbf{23}}
        Secunda conclusio: quod licet theologica propositio sit theologica cognitio, tamen non est necesse notitiam, qua quis dicitur scire totale obiectum sui ipsius esse. Prima pars probatur quia fides vel notitia theologica non differt ab assensu theologico. Sed assensus theologicus esse propositio theologica, ergo propositio theologica est notitia seu cognitio theologica. Maior patet quia fides theologica non potest esse sine assensu theologico, alias quis crederet verum et non assentiret illi quod est falsum. Minor patet ex prima conclusione. Secunda pars probatur quia nihil causatur a se ipso, sed scientia nostra saltim partialiter causatur ab obiecto, ergo scientia non est suum obiectum totale.
        \pend
     
        \pstart
        \ledsidenote{\textbf{24}}
        Confirmatur quia, dato quod visio sit visio sui, tamen requirit aliud obiectum a quo causetur. Sic supposito quod idem sui ipsius sit notitia, nihilominus aliud obiectum requiritur a quo talis notitia causetur, et, per consequens, non est suum totale obiectum, quod erat probandum.
        \pend
     
        \pstart
        \ledsidenote{\textbf{25}}
        Primum corollarium: quod propositiones theologicae non sunt obiectum Sacrae Scripturae.
        \pend
     
        \pstart
        \ledsidenote{\textbf{26}}
        Secundum corollarium: quod theologia vel alia scientia non proprie scitur, sed est id quo aliquid scitur vel cognoscitur. Paret quia illud quod scitur est obiectum scientiae, sed scientia non est obiectum sui ipsius, saltim totale, ergo.
        \pend
     
        \pstart
        \ledsidenote{\textbf{27}}
        Tertium corollarium: quod ista propositio de virtute sermonis est falsa \enquote*{\name{Socrates}\index[persons]{Sortes} scit geometriam, logicam, theologiam vel aliquam aliam scientiam.} Patet ex eodem fundamento: quia scientia non proprie scitur, sed est id quo aliquid scitur sic esse vel non esse.
        \pend
      
        \bigskip
         \section*{[Tertio Conclusio]} 
        \pstart
        \ledsidenote{\textbf{28}}
        Tertia conclusio: licet non repugnet viatori nullam veritatem theologicam credere et actum et habitum fidei habere, tamen stante lege repugnat viatorem sine speciale lumine veritatibus theologicis assentire.
        \pend
      
        \pstart
        \ledsidenote{\textbf{29}}
        Prima probatur primo de habitu. Non est dubium quin multi habent habitum virtutum sine operari. Sed quod actus fidei possit esse sine credere probatur quia non plus identificantur actus fidei et suum actuare vel agere quam accidens et suum inhaerere, sed Deus potest facere accidens sine inhaerere, ergo.
        \pend
      
        \pstart
        \ledsidenote{\textbf{30}}
        Secunda pars probatur per \name{Augustinum}\index[persons]{Augustinus} in quadam epistola, ubi inquit: \edtext{\enquote{[Neque enim] cum coepero te in tanti huius secreti intelligentiam introducere (nisi Deus intus ad iuverit, non potero)}}{\lemma{Neque \dots\ potero)}\Afootnote{PL 33:453}} et alibi in locis pluribus. Item \name{Anselmus}\index[persons]{Anselm} I \worktitle{Cur Deus Homo}\index[works]{Cur Deus homo}, capitulo secundo: \edtext{\enquote{Rectus ordo exigit ut profunda Christianae fidei, prius credamus, quam ea ratione fidei praesumamus discutere.}}{\lemma{Rectus \dots\ discutere.}\Afootnote{\name{Anselmus}\index[persons]{}, \worktitle{Cur Deus Homo}\index[works]{Cur Deus homo}, I, c. 2}} Item \name{Augustinus}\index[persons]{Augustinus} in \worktitle{Homilia prima super Iohannem}\index[works]{Homilies on John}, dicens, \edtext{\enquote{Deus illuminat parvulos et indoctos lumine fidei, maiores autem lumine sapientiae,}}{\lemma{Deus \dots\ sapientiae,}\Afootnote{Augustinus, xxx}} ergo ad scientifice assentiendum veritatibus theologicis requiritur speciale lumen.
        \pend
     
        \pstart
        \ledsidenote{\textbf{31}}
        Primum corollarium: plures salvabuntur finaliter qui numquam crediderunt nec credent actualiter. Patet de pueris post baptismum statim decedentibus. Item de baptizatis rationis usu carentibus, sicut sunt fatui nati.
        \pend
     
        \pstart
        \ledsidenote{\textbf{32}}
        Secundum corollarium: multi sunt beati in regno caelorum qui, si credidissent, fuissent damnati et de numero reproborum aut in loco damnatorum. Patet de pueris Graecorum statim \ledsidenote{L13v} post baptismum ante annos discretionis decedentium. Nam constat quod Graeci baptizati, licet sub alia forma verborum et per consequens recipiunt gratiam qua mediante possunt salvari, tamen quia adulti eorum aliter credunt quam Roma Ecclesia credit et credere praecipit, ideo non sunt in statu salutis.
        \pend
     
        \pstart
        \ledsidenote{\textbf{33}}
        Tertium corollarium: haec est possibilis, lege stante, viator non credit sicut ecclesia credere praecipit, et tamen viator in credendo sic nullo peccat seu deficit vel sic stat aliquem viatorem in credendo non culpabilem deficere, et tamen ipsum, sicut ecclesia iubet credendum, non credere. Patet, quia stat aliquem licite pro aliquo tempore nihil credere, ergo stat talem non errare nec credere sicut Roma Ecclesia credit. Ex dictis patet pars negativa quovis vera.
        \pend
     
        \pstart
        \ledsidenote{\textbf{34}}
        Contra primam conclusionem arguitur primo sic: propositio theologica potest esse sine assensu theologico, ergo illa distinguitur. Consequentia bona. Antecedens probatur quia omnem propositionem quam potest formare fidelis potest formare infidelis stante infidelitate. Et cum talis non habeat assensum veri theologiae, sequitur quod illa propositio theologica ab eo formata non est intrinsece assensus.
        \pend
     
        \pstart
        \ledsidenote{\textbf{35}}
        Secundo sequitur quod si quis theologus fieret infidelis vel haereticus ipse perderet totam scientiam theologicam. Consequentia patet quia perderet assensum propositionum theologicarum et talis assensus est theologica scientia. Sed falsitas probatur, quia talis adhuc sciret defendere fidem nostram legere et disputare et argumenta solvere sicut prius, immo non minus sciret quam prius.
        \pend
     
        \pstart
        \ledsidenote{\textbf{36}}
        Tertio per assensum assentimus propositioni, igitur assensus theologicus non est propositio theologica. Consequentia patet, quia per propositionem non assentimus propositioni, quia si sic idem esset id quo assentimus et cui assentimus.
        \pend
     
        \pstart
        \ledsidenote{\textbf{37}}
        Quarto stat illam propositionem esse et eius assensum non esse, ergo. Probatur antecedens quia aliquis potest credere aliqua propositionem esse veram et postea sed non oppositum propter aliquas rationes eadem propositione manente.
        \pend
     
        \pstart
        \ledsidenote{\textbf{38}}
        Quinto contra secundam partem arguitur quia si assensus diversificaret speciem propositionis, si aliquis formaret illam \enquote*{nullus homo est asinus,} sequitur quod nullus dissentiens illi posset formare propositionem eiusdem speciei. Patet consequentia ex terminis. Falsitas probatur quia dissentiens illi potest formare propositionem eiusdem subiecti et praedicati, ergo eiusdem speciei. Consequentia patet cum propositiones non distinguuntur a suis terminis similis sumptis.
        \pend
     
        \pstart
        \ledsidenote{\textbf{39}}
        Sexto contra corollaria quia vel fides et theologia sunt idem habitus vel non. Si sic, ergo habens habitum theologicum habet fidem et per consequens est theologus. Si diversi, ponatur quod remittatur uniformiter aequaliter praecise quoad omnia versus non esse, tunc vel aequaliter desinent non esse vel non. Si primum, ergo sunt aequales et per consequens theologia non est superior fide quod est falsum et contra \name{Augustinum}\index[persons]{Augustinus} XIV \worktitle{De Trinitate}\index[works]{De Trinitate} capitulo primo dicens, \edtext{\enquote{hac scientia, scilicet theologia, non pollent plurimi quamvis fide polleant.}}{\Afootnote{Augustinus, De Trinitate, XIV, c. 1, xxx}} Si detur secundum, sequitur quod theologia stabit sine fide quod repugnat dictis.
        \pend
     
        \pstart
        \ledsidenote{\textbf{40}}
        Septimo contra secundam partem secundae conclusionis: omne quod scitur est verum, sed sola propositio est vera, ergo sola propositio scitur, ergo sola propositio est totale obiectum nostrae scientiae.
        \pend
     
        \pstart
        \ledsidenote{\textbf{41}}
        Octavo contra corollaria eiusdem conclusionis quia vel obiectum theologiae totale est propositio theologica vel res ad extra vel significatum propositionis. Si primum habetur contra primum corollarium. Non secundum quia tunc multae scientiae essent de rebus contingentibus potentibus \ledsidenote{L14r} aliter se habere tamquam de obiectis contra \edtext{\name{Philosophum}\index[persons]{Aristotle} VI \worktitle{Ethicorum}\index[works]{Ethics}}{\Afootnote{Aristoteles, Ethica, VI, xxx}} et \edtext{I \worktitle{Posteriorum}\index[works]{Posterior Analytics}.}{\Afootnote{Aristoteles, Posteriora Analytica}} Nec tertium eadem radice, quia significat propositio rem extra animam.
        \pend
     
        \pstart
        \ledsidenote{\textbf{42}}
        Confirmatur quia eadem ratione res ad extra esset obiectum opinionis fidei et erroris, et per consequens staret quod unus et idem homo sciret et opinaretur crederet et ignoraret quae sunt falsa. Patet consequentia nam stat hominem scire quod Deus est trinus et unus, opinari quod ipse solus moveat caelum, credere quod sit incarnatus, errare putando ipsum esse finiti vigoris.
        \pend
     
        \pstart
        \ledsidenote{\textbf{43}}
        Nono contra tertiam conclusionem: veritates theologicae sunt per se notae vel ex per se notis deductae, ergo ad earum notitiam fides non est necessaria. Antecedens probatur quia nullus teneretur indubitantur credere aliquid quod non est sibi per se notum vel ex per se notis deductum, sed quilibet tenetur indubitanter credere veritates theologicas, ergo. Maior patet quia omne verum est deductum vel deducibile ex primo principio cum quodlibet tale virtualiter in eo contineatur.
        \pend
     
        \pstart
        \ledsidenote{\textbf{44}}
        [Decimo], si tale lumen etc., sequeretur quod quilibet teneretur habere maiorem certitudinem de talibus veritatibus quam sit certitudo scientiae consequentium. Sed patet consequentia quia certitudo fidei est infinita cum infinite distet ab omni gradu dubietatis.
        \pend
     
        \pstart
        \ledsidenote{\textbf{45}}
        Undecimo, nullum lumen speciale requiritur ad assentiendum mendacio, ergo nec aliter vero theologico, quia dicens verum theologicum quod putat esse falsum dicit mendacium.
        \pend
     
        \pstart
        \ledsidenote{\textbf{46}}
        Ad rationes. Ad primum, dico quod propositio theologica potest sumi duplex. Uno modo pro quacumque propositione significante rem theologicam, et sic pura propositio vocalis potest dici theologica. Alio modo sumitur pro illa propositione quae intrinsece est assensus theologicus, et sic dico quod talis propositio quae intrinsece est significatum significati theologici non potest esse sine assensu theologico. Et ad probationem negatur assumptum accipiendo propositionem theologicam secundo modo, licet possit formare omnem propositionem significantem ad placitum.
        \pend
     
        \pstart
        \ledsidenote{\textbf{47}}
        Ad secundum, conceditur consequens capiendo scientiam prout includit assensum theologicum, sed capiendo scientiam pro habitu quo quis potest scire concludere unam propositionem ex alia sic non perderet suam scientiam, sed tamen talis non deberet dici theologus.
        \pend
     
        \pstart
        \ledsidenote{\textbf{48}}
        Ad tertium, negatur antecedens, et ad probationem dicitur quod ille assensus est met propositio qua assentimus non propositioni, sed sic esse vel non sic esse ex parte rei.
        \pend
     
        \pstart
        \ledsidenote{\textbf{49}}
        Ad quartum, negatur antecedens, et ad probationem dico quod quamdiu manebit illa propositio et assensus etiam quamvis una propositio quodammodo similis ei poterit manere quae non erit assensus, et tunc illa propositio non est theologica, et sic potest dici quod illa propositio manebit, sed non manebit propositio.
        \pend
      
        \pstart
        \ledsidenote{\textbf{50}}
        Ad quintum, conceditur consequens, et ad probationem eius conceditur antecedens, sed negatur consequentia, quia propositio quae est assensus ultra terminos includit aliud, scilicet, assensum qui non includitur in altera. Secundo potest dici quod copula unius non est eiusdem speciei cum copula alterius, quia modus coniungendi subiectum cum praedicato est diversus eo quia una propositio est assensus et non alia, sed solum apprehensio.
        \pend
     
        \pstart
        \ledsidenote{\textbf{51}}
        Ad sextum, dicitur quod non sunt unus habitus et cum ponitur quod remittantur uniformiter forte quidam dicerent quod uniformiter remitti non possunt, sed admisso casu dicitur quod remanebit aliquis gradus theologiae, sed non deberet dici theologia, quia sine fide non sufficeret ille habitus habentem denominare theologicum. Et sic potest dici \ledsidenote{L14v} conformiter ad praedicta quod theologia potest manere sine fide, sed non potest manere sine fide theologia, non enim sufficeret ille habitus habentem theologicum denominare saltim secundum legem praesentem. Nota quod \name{Alphonsus}\index[persons]{} tenet quod sit idem habitus theologiae et fidei, quaestione tertia prologi, articulo secundo, conclusione prima, sed \name{Thomas Argentiae}\index[persons]{Thomas of Strasbourg} tenet oppositum et eius rationes solvit \edtext{quaestione quarta, articulo secundo.}{\Afootnote{Thomas de Argentina, I, q. 4, a. 2}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{52}}
        Ad septimum, quod est contra secundam conclusionem, negatur minor quia multa sunt vera quae non sunt propositiones sicut significata propositioni. Et hoc videtur de mente \edtext{\name{Aristotelis}\index[persons]{Aristotle} libro \worktitle{Praedicamentorum}\index[works]{Categories} capitulo de oppositis,}{\Afootnote{Aristoteles, Praedicamentorum, xxx}}  ubi dicit quod significatum unius propositionis contradictione contradicit significato alterius, et si unum est verum, reliquum est falsum, ista autem significata quadam extrinseca denominatione Deum vera vel falsa, quia eorum significata sunt vera vel falsa vel essent si formarentur, et sic potest dici quod idem significatum est verum quod est significabile per veram propositionem et illud falsum quod per falsam. Item potest dici quod omnia vera significabilia possunt dici vera a prima veritate quae Deus est qui est omnium talium verum et certum iudicium.
        \pend
     
        \pstart
        \ledsidenote{\textbf{53}}
        Ad octavum, dicitur quod neutrum praedictorum, sed est sic esse vel non esse, et si quaeratur, utrum sit illud aliqua res dicitur quod sic sumendo \enquote*{rem} large pro omni significabili complexe vel incomplexe vere vel false. Et cum dicitur quod de rebus contingentibus esset scientiam, etc. hic dicitur quod non, ideo dicuntur scientiae necessariis quia eorum obiecta sint quaedam res et entitates reales necessariae, quia sic nulla est nisi Deus, sed dicuntur de necessariis quae ea quae sciuntur non contingunt non sic esse. Verbi gratia, hoc scibile omnis isoceles habet tres angulos etc. Non dicitur necessarium, quia necesse est quod sit isoceles vel quod necessario habeat tres etc., sed quia necesse est si isoceles est quod habeat res angulos, etc. Etiam illud scibile dicitur aeternum, quia quandoque isoceles fuit, habuit tres, et quandoque existat, semper habebit tres.
        \pend
     
        \pstart
        \ledsidenote{\textbf{54}}
        Ad confirmationem posset concedi consequens quia eundem Deum scimus esse et credimus terminum et forte possumus errare circa eum tenendo aliquam opinionem falsam, et hoc secundum diversas considerationes et habitudines.
        \pend
     
        \pstart
        \ledsidenote{\textbf{55}}
        Ad aliam, negatur antecedens loquendo de notitia viatorum et ad eius probationem dicitur quod maior est falsa nisi reciperetur per se notum, id est propter nihil ad certius assertum sicut videtur assumere \name{Altissiodorensis}\index[persons]{William of Auxerre} libro tertio \worktitle{Summae}\index[works]{} suae dicens quod \edtext{\enquote{articuli fidei sunt per se noti.}}{\Afootnote{Guillelmus Altissiodorensis, xxx}} Nota differentiam de per se noto quam ponit \edtext{\name{Hugolinus}\index[persons]{Hugolino of Orvieto} libro primo, distinctione tertia, quaestione prima, articulo primo.}{\Afootnote{Hugolinus de Urbe Veteri, xxx}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{56}}
        Ad aliam, negatur consequentia. Ad probationem negatur quod certitudo fidei sit infinita, immo nulla certitudo creata est infinita, quia quaelibet talis est actus vel habitus finitus ad actum finitum inclinans.
        \pend
     
        \pstart
        \ledsidenote{\textbf{57}}
        Ad ultimum negatur maior quia aliquis in casu tenetur credere et assentire mendacio sine haesitatione, non tamen inquantum mendacium, sed praecise contra mentem dicentis et putantis falsum quod est verum.
        \pend
     
        \pstart
        \ledsidenote{\textbf{58}}
        Ad primum rationem factam ante oppositum, conceditur maior et conclusio quae est indefinita quia intellectus naturaliter potest intelligere aliquid. Ad confirmationem negatur consequentia. Ad probationem iterum negatur consequentia. Et ad eius probationem etiam negatur consequentia, quia non aeque primo nec eodem modo ratio eorum movet intellectum. De hoc vide \edtext{\name{Henricum de Gandavio}\index[persons]{Henry of Ghent} parte prima articulo primo 44 14 quaestionibus.}{\Afootnote{Henricus de Gandavensis, xxx}} 
        \pend
     
        \pstart
        \ledsidenote{\textbf{59}}
        Ad aliam quidquid sit de antecedente, negatur consequentia. Et ad probationem dicitur illud esse verum de totali agente et praeciso, sed intellectus agens non est totale agens ipsam intellectionem causans, ideo non concludit.
        \pend
     
        \pstart
        \ledsidenote{\textbf{60}}
        Ad aliud conceditur consequens. Et ad improbationem, negatur consequentia quia sic probaretur quod nullus esset habitus caritatis vel gratiae infusus creaturae quia non omnes qui habent percipiunt, immo nemo sic in ordio vel amore dignus sit. Secundo posset dici quod omnis \ledsidenote{L15r} theologus percipit, licet non perfecte percipiat ipsum, nec percipiat se percipere.
        \pend
     
        \pstart
        \ledsidenote{\textbf{61}}
        Ad ultimum posset negari consequentia quia si potest convincere sine tali lumine, non tamen taliter vel modo tam perfectio sicut ponit fides vel theologia, et sic posset dicit ad primum praecedens. Secundo potest negari antecedens et dici quod probationes adiutae non probant quia simile probaretur quod homo ex puris naturalibus posset sequi beatitudinem quod est falsum.
        \pend
       
        \endnumbering
        
     
        \end{document}
    